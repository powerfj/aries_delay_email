# 邮件项目

## 总体设计思路

1.  web前端页面对输入进行合法性校验，减少后端的压力，提高用户体验
2.  web后段接收到请求也会做合法性校验
3.  合法性校验没问题之后把邮件和附件写入本地的两个文件夹 mails和attachement。先把附件保存在attachment文件夹，文件名是使用上传上来的文件作为key,key有两个部分，一部分是部分内容+名字+时间的md5 hash，保证时间的唯一性，后半部分使用上传使用过来的文件名，这个后续发邮件的时候可以用到。
4.  邮件内容以及附件存储在本地的key会序列号成一个pickle文件存储在mails文件夹，pickle文件的文件名字是一个md5 hash
5.  本地有一个进程(subprocess启动)随web进程启动，监视mails文件夹，如果有新文件，就启动对于邮件的处理，这个是因为如果直接在web进程做的话，会因为s3上传附件的时候导致web进程会卡比较久才返回。处理逻辑就是读取pickle 邮件，反序列号出来之后看发送的时间是否超过当前的时间，如果不超过就直接采用这个进程发送了，发送完就清理mail以及attachment。如果不是就把附件写入amazone s3，邮件内容使用delay_seconds写入sqs
6.  本地还有一个进程(可以作为worker，但是因为目前只有一个dyno就使用的subprocess启动)，这个进程就是专门读取sqs的内容，如果发送时间比现在还延后，就继续删除sqs的这条消息，然后延迟写入一条新的sqs消息。如果发送时间比现在早，那就对这条内容进行发送。

## 目前有一些实现的一些缺陷

1.  写入sqs的消息内容格式应该与保存在本地的文件的格式一致，都使用pickle，这样可以减少格式的转换。
2.  对于一些非常特殊的情况可能没有考虑全面

## 测试用例

### 前端界面测试用例

1.  用例1
收件人为空，是否有提示

2.  用例2
收件人为非法邮箱地址，是否有提示

3.  用例3
标题为空，是否会有错误提示

4.  用例4
文本邮件的时候，内容为空，是否会有提示

5.  用例5
富媒体邮件的时候，内容为空，是否会有提示


### 邮件发送测试用例

1.  用例1
内容: 发送文本内容邮件，不带附件
期待输出:对应邮箱收到对应的邮件内容

2.  用例2
内容: 发送html格式内容邮件(邮件内容包括一个链接)，不带附件
期待输出: 对应邮箱收到邮件，邮件格式是html的(能打开链接)

3.  用例3
内容: 指定时间发送邮件，不带附件，指定时间包括15分钟之内，15分钟，以及15分钟之后
期待输出: 在指定时间收到邮件，对比邮箱里面收到邮件的时间，附件也收到

4.  用例4
内容:指定延时发送邮件，不带附件，延时包括1分钟之内，15分钟，20分钟三种
期待输出：在指定延时的时间收到邮件，对比邮箱里面收到邮件的时间，附件也收到

5.  用例5
内容: 发送邮件附带文本文件附件发送邮件
期待输出: 收到邮件，附件内容与发送的附件md5一致

6.  用例6
内容：发送邮件附带二进制文本附件发送邮件
期待输出：收到邮件，附件内容与发送的附件md5一致

7.  用例7
内容：发送邮件附带中文名字附件发送邮件
期待输出：收到邮件，附件名字与发送时候的名字一致，能显示出来

8.  用例8
内容: 发送收件人设置为多个收件人(;)分隔
期待输出：多个收件人都收到邮件


## 项目地址以及如何部署

1.  git@bitbucket.org:powerfj/aries_delay_email.git
2.  因为有一个config.json保存了s3以及sqs的登录的密钥，所以没有提交到master，但是会在自己的一个deploy的分支
3.  部署的时候使用了 git push heroku deploy:master 的方式

## 项目过程的一些记录

### 项目完成过程

1.  安装virtualenv

    sudo pip install virtualenv

2.  切换到virtualenv

    . env/bin/activate

3.  在virtualenv里面安装webapp2安装[webapp2](https://webapp-improved.appspot.com/tutorials/quickstart.nogae.html#tutorials-quickstart-nogae)

    pip install WebOb
    pip install Paste
    pip install webapp2

4.  [amazon sqs参考](http://docs.aws.amazon.com/zh_cn/AWSSimpleQueueService/latest/SQSGettingStartedGuide/Introduction.html)
5.  [amazon sqs introduction](http://boto.readthedocs.org/en/latest/sqs_tut.html)
    
6.  富文本编辑器用的tower的 [simditor](http://simditor.tower.im/)


### heroku项目部署

1.  申请帐号，安装virturlenv [链接](https://devcenter.heroku.com/articles/getting-started-with-python#introduction)
2.  下载Heroku Toolblet，然后heroku login [链接](https://devcenter.heroku.com/articles/getting-started-with-python#set-up)
3.  准备好自己的app，这里是让你下载一个app[链接](https://devcenter.heroku.com/articles/getting-started-with-python#prepare-the-app)
4.  使用pip freeze > requirements.txt 来生成requirements.txt
5.  heroku Procfile撰写，前面是类型，后面是命令，然后如果是web类型，需要用$PORT来指定端口，直接绑定80是不行的，还有就是服务器地址一定
要绑定0.0.0.0，如果绑定的127.0.0.1好像死活是不行的

    web: python main.py $PORT
    worker: python -m api.mail

6.  因为免费的dyno只有一个，所以使用subprocess的方式而不是在Procfile中写worker，heroku官方也是可以使用这个方式的，但是实际运行中这种方式可能不大好扩展，需要权衡使用
7.  git push heroku deploy:master
8.  这样就可以部署上去了


### heroku项目命令收集

1.  heroku open 打开heroku网站
2.  heroku ps 看打开了几个正在运行的dynos
3.  heroku logs 看log
4.  heroku ps:scale web=0 #关闭heroku 
5.  heroku ps:scale web=1 #关闭heroku 
6.  heroku run bash #在heroku机器上面打开bash
7.  heroku config:set TZ=Asia/Hong_Kong #配置heroku的时区


### 开发过程中遇到的几个问题
1.  heroku貌似不大能用作静态文件的服务器，这里最好是静态服务器用第三方的cdn，这里我目前把静态文件放在我自己的一个vps上面，速度可能略慢，而且也没有https
2.  heroku Procfile撰写，前面是类型，后面是命令。如果是web类型，命令需要接受端口参数，因为heroku启动的时候会通过$PORT来设定端口，直接绑定80是不行的。还有服务器地址一定要绑定0.0.0.0，如果绑定的127.0.0.1就是接收不到请求
3.  heroku使用worker来生命后台进程的时候，会与web 不是同一个dyno，应该是为了好扩展。但是因为免费的目前只有一个，所以用subprocess的方式来在web dyno上面起多个进程，这样就减少了dyno的使用，当然扩展的时候性能可能不一定好
4.  我这边分配到heroku默认的时区好像是0，但是时间又和东八区时间是一样的，所以导致设置指定时间去发送的时候，因为时区设置会有时间偏差，所以就用上面的方法设置了heroku的时区
5.  如果要学会使用heroku来进行部署真实的服务，heroku dyno的一些特性需要多熟悉熟悉[dyno manager](https://devcenter.heroku.com/articles/dynos)
6.  中间加入的库需要重新用pip freeze > requirements.txt 来生成新的 requirements.txt
