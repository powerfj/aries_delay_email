#encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import webapp2
from webapp2_extras import jinja2
import time
from api.models import MailAttachment,MailObject
import logging
import traceback
logging.basicConfig(level=logging.DEBUG)
import hashlib
import os
from validate_email import validate_email
import json

"""
## 总体设计思路

1.  web前端页面对输入进行合法性校验，减少端的压力
2.  web接收到请求也会做合法性校验
3.  合法性校验没问题之后把邮件和附件写入本地的两个文件夹 mails和attachement。先把附件保存在attachment文件夹，文件名是使用上传上来的文件作为key,key有两个部分，一部分是部分内容+名字+时间的md5 hash，保证时间的唯一性，后半部分使用上传使用过来的文件名，这个后续发邮件的时候可以用到。
4.  邮件内容以及附件存储在本地的key会序列号成一个pickle文件存储在mails文件夹，pickle文件的文件名字是一个md5 hash
5.  本地有一个进程(subprocess启动)随web进程启动，监视mails文件夹，如果有新文件，就启动对于邮件的处理，这个是因为如果直接在web进程做的话，会因为s3上传附件的时候导致web进程会卡比较久才返回。处理逻辑就是读取pickle 邮件，反序列号出来之后看发送的时间是否超过当前的时间，如果不超过就直接采用这个进程发送了，发送完就清理mail以及attachment。如果不是就把附件写入amazone s3，邮件内容使用delay_seconds写入sqs
6.  本地还有一个进程(可以作为worker，但是因为目前只有一个dyno就使用的subprocess启动)，这个进程就是专门读取sqs的内容，如果发送时间比现在还延后，就继续删除sqs的这条消息，然后延迟写入一条新的sqs消息。如果发送时间比现在早，那就对这条内容进行发送。

## 目前有一些实现的一些缺陷

1.  写入sqs的消息内容格式应该与保存在本地的文件的格式一致，都使用pickle，这样可以减少格式的转换。
2.  对于一些非常特殊的情况可能没有考虑全面
3.  页面发送邮件和附件可以做成ajax方式的，体验会更好
"""

class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(app=self.app)

    def json_resp(self,code,msg,**kwargs):
        self.response.write(self.json_output(code,msg,**kwargs))

    def json_output(self,code,msg,**kwargs):
        rst=kwargs
        if msg:
            rst["msg"]=msg
        rst["err"]=code
        return json.dumps(rst)

    def jsonp_resp(self,funname,code,msg,**kwargs):
        self.response.write('<script>%s(%s);</script>'%(funname,self.json_output(code,msg,**kwargs)))

    def render_response(self, _template, **context):
        # Renders a template and writes the result to the response.
        rv = self.jinja2.render_template(_template, **context)
        self.response.write(rv)

class SendMailHandler(BaseHandler):

    def get(self):
        self.render_response('sendmail.html')

    def check_time(self,post_obj):
        if post_obj.get('delay_type')=='now':
            return True,0
        elif post_obj.get('delay_type')=='delay':
            try:
                days = int(post_obj.get('delay_days') or 0)
                hours = int(post_obj.get('delay_hours') or 0)
                minutes = int(post_obj.get('delay_minutes') or 0)
                if days>=0 and hours>=0 and minutes>=0:
                    return True,((days*24 + hours)*60 + minutes )*60
            except:
                logging.error(traceback.format_exc())
                return False,"延迟时间有误"
        elif post_obj.get('delay_type')=='specify':
            try:
                #delay_time = time.strptime(post_obj.get('delay_send_time'),"%Y/%m/%d %H:%M")
                #秒数
                delay_time_gmt = int(post_obj.get('delay_send_time_gmt',0))
                delay_sec = delay_time_gmt - time.mktime(time.gmtime())
                print "time : %s,%s,%s"%(delay_time_gmt,time.mktime(time.gmtime()), delay_sec)
                if delay_sec>=0:
                    return True,int(delay_sec)
                return False,"指定发送时间比当前时间早"
            except:
                return False,"指定发送时间格式错误"
        return False,"未指定延迟发送时间"

    def check_email(self,mailto):
        return all([validate_email(x.strip()) for x in mailto.split(';')])

    def post(self):
        post_obj = self.request.POST
        flash = {}
        delay_time_err,delay_result = self.check_time(post_obj)
        valid = True
        if not post_obj.get('mailto'):
           flash['msg']='收件人参数为空'
           valid = False
        if not self.check_email(post_obj.get('mailto')):
           flash['msg']='收件人邮件地址不合法'
           valid = False
        elif not post_obj.get('title'):
           flash['msg']='邮件标题为空'
           valid = False
        elif post_obj.get('content_type')=="txt" and not post_obj.get('content'):
           flash['msg']='邮件内容为空'
           valid = False
        elif post_obj.get('content_type')=="html" and not post_obj.get('content_html'):
           flash['msg']='邮件内容为空'
           valid = False
        elif not delay_time_err:
           flash['msg']=delay_result
           valid = False
        attach =  post_obj.get('attachment')
        attachments = None
        if attach != None and hasattr(attach,'file'):
            file_data= attach.file.read()
            file_name = attach.filename;
            attach_obj = MailAttachment(file_name,file_data)
            attachments = [ attach_obj.save_to_local() ]
            print "save file to local"
        else:
            print "no attach ment"
        if valid:
            content_type =  post_obj.get('content_type')
            if content_type =="txt" :
                content = post_obj.get('content')
            if content_type =='html':
                content = post_obj.get('content_html')
            mailobj = MailObject(
                title=post_obj.get('title'),
                content=content,
                send_time=int(delay_result+time.time()),
                mailto=post_obj.get('mailto'),
                content_type=content_type,
                attachments = attachments
            )
            mailobj.write_to_local()
            flash['msg']="邮件发送成功"
            self.jsonp_resp("parent.on_post_resp",0,flash["msg"])
        else:
            self.jsonp_resp("parent.on_post_resp",-1,flash["msg"])


app = webapp2.WSGIApplication([
    ('/', SendMailHandler)
], debug=True)

def start_child():
    import subprocess
    import os
    working_path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(working_path)
    child_mail=subprocess.Popen(["python","-m","api.mail"])
    child_mail_writer=subprocess.Popen(["python","-m","api.mail_writer"])

def main():
    from paste import httpserver
    import os
    start_child()
    if not os.path.exists('./mails/'):
        os.mkdir('./mails/')
    if not os.path.exists('./attachment/'):
        os.mkdir('./attachment/')
    port = int(os.environ.get('PORT', 8080))
    httpserver.serve(app, '0.0.0.0', port=port)

if __name__ == '__main__':
   main()
