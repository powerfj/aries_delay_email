#encoding=utf8
import json
import logging

"""
config value may include secrete key or passwords we didn't have to add to the cvs
we just declear what *config key* we need in the CONFIG_KEYS, and we set the config 
value in json files

MANDRILL_NAME: 
    mandrill smtp user name

MANDRILL_PASS: 
    mandrill smtp passowrd

SQS_KEY:
    amazon sqs access key

SQS_SECRET:
    amazon sqs access secret

"""
CONFIG_KEYS=['MANDRILL_NAME','MANDRILL_PASS','SQS_KEY','SQS_SECRET']

def load_config_from_file(local_obj):
    json_data = json.load(open('config.json'))
    for key in CONFIG_KEYS:
        if None==json_data.get(key):
            logging.warn('config key %s not setted'%key)
        local_obj[key]=json_data.get(key)

load_config_from_file(locals())
