#encoding=utf8

import smtplib
from email.utils import formatdate
from email.Header import Header 
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from config import MANDRILL_NAME,MANDRILL_PASS
from email import encoders

def send_by_mandrill(receiver,subject,content ,msg_type="txt",attachments=None):
    sender='powerfj@gmail.com'
    if type(receiver)==str or type(receiver)==unicode:
        receiver=[x.strip() for x in receiver.split(';')]
    smtpserver = 'smtp.mandrillapp.com'
    username = MANDRILL_NAME
    password = MANDRILL_PASS

    if msg_type=="html":
        msg = MIMEMultipart('alternative')
        htm_text = MIMEText(content,'html','utf-8')
        msg.attach(htm_text)

    elif msg_type=="txt":
        msg = MIMEMultipart('alternative')
        txt_text = MIMEText(content,'plain','utf-8')
        msg.attach(txt_text)

    msg['Subject'] = Header(subject,'utf-8')
    msg['From']=sender
    msg['To']=",".join(receiver)

    attachments = attachments or []

    for attach in attachments:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( attach.data)
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'%attach.filename)
        msg.attach(part)

    smtp = smtplib.SMTP()
    smtp.connect(smtpserver,587)
    if username:
        smtp.login(username,password)
    smtp.sendmail(sender, receiver, msg.as_string())
    smtp.quit()

def main():
    send_by_mandrill('powerfj@gmail.com',"haha","email test",msg_type="html")

if __name__=="__main__":
    main()
