#encoding=utf8
import hashlib

def get_md5_digest(data):
    md5 = hashlib.new("md5")
    md5.update(data)
    md5str = md5.hexdigest()
    return md5str
