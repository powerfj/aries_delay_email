#encoding=utf8

from api import mail
from api import md5
from api import s3store
import pickle
import time
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler
from watchdog.events import FileCreatedEvent
from api import sendemail
from api.models import MailAttachment,MailObject
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os
import logging
import traceback


def process_local_mail_file(path):
    mail_obj = MailObject.read_from_file(path)
    process_local_mail(mail_obj)
    try:
        os.system("rm '%s'"%path)
    except:
        logging.info(traceback.format_exc())
    if mail_obj.attachments:
        for x in mail_obj.attachments:
            MailAttachment.delLocalFileByKey(x)
    logging.info('delete mail and attachments for file:%s'%path)

def process_local_mail(mail_obj):
    send_time = mail_obj.send_time
    delay_seconds = max(0,min(int(send_time-time.time()),15*60))
    attachments = None
    print mail_obj.attachments
    if mail_obj.attachments:
        attachments = [MailAttachment.fromFileKey(x) for x in mail_obj.attachments]
    if delay_seconds < mail.NO_DELAY_SECONDS:
        logging.info('sending mail from local')
        sendemail.send_by_mandrill(
                mail_obj.mailto,
                mail_obj.title,
                mail_obj.content,
                mail_obj.content_type,
                attachments=attachments)
    else:
        json_attach = None
        if attachments:
            for attach in attachments:
                s3store.save_file_to_s3(attach.key,attach.data)
                print "store attachment to s3:%s"%attach.filename
        mail.write_mail_to_sqs(
                mail_obj.title,
                mail_obj.content,
                mail_obj.send_time,
                mail_obj.mailto,
                mail_obj.content_type,
                mail_obj.attachments
        )
        logging.info("send mail to sqs")
    

class MailFileEventHandler(FileSystemEventHandler):

    def on_created(self,event):
        if isinstance(event,FileCreatedEvent):
            logging.info('on file created:%s'%event.src_path)
            process_local_mail_file(event.src_path)

def watch_mail_created():
    event_handler = MailFileEventHandler()
    observer = Observer()
    observer.schedule(event_handler, './mails/', recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

def process_mail_files():
    unsend_mails = os.listdir('./mails')
    while unsend_mails:
        for x in unsend_mails:
            process_local_mail_file('./mails/%s'%x)
        unsend_mails = os.listdir('./mails')

if __name__ == "__main__":
    process_mail_files()
    watch_mail_created()
    #process_local_mail_file(sys.argv[1])
