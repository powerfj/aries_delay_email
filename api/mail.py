#encoding=utf8
from config import SQS_KEY
from config import SQS_SECRET
import boto
from boto.sqs.message import Message
import time
import sendemail
import traceback
import logging
logging.basicConfig(level=logging.INFO)
import json
from api.models import MailAttachment,MailObject
from api import s3store

"""
## 参考

    Using AWS S3 to Store Static Assets and File Uploads
    https://devcenter.heroku.com/articles/s3

    sqs开发人员指南
    http://awsdocs.s3.amazonaws.com/SQS/latest/sqs-dg-zh_cn.pdf


    sqs python 指南
    http://boto.readthedocs.org/en/latest/sqs_tut.html

## 消息写入与消息的message_attribute例子

message_attribute 需要 data_type 和 string_value两个字段

    m = Message()
    m.message_attributes = {
        "name1": {
            "data_type": "String",
            "string_value": "I am a string"
        },
        "name2": {
            "data_type": "Number",
            "string_value": "12"
        }
    }

    rs = queue.get_messages(message_attributes=['name1', 'name2'])

"""

#不需要再延迟的秒数
NO_DELAY_SECONDS=3

g_queue = None

def get_sqs_email_queue():
    global g_queue
    if g_queue:
        return g_queue
    conn = boto.sqs.connect_to_region( 
           "us-west-2",
           aws_access_key_id=SQS_KEY,
           aws_secret_access_key=SQS_SECRET)
    g_queue = conn.get_queue('aries_email')
    return g_queue

def write_mail_to_sqs(title,content,send_time,mail_to,content_type='txt',attachments=None):
    q = get_sqs_email_queue()
    m = Message()
    m.set_body(content)
    m.message_attributes = {
            "send_time":{
                "data_type":"Number",
                "string_value":send_time
            },
            "mail_to":{
                "data_type":"String",
                "string_value":mail_to
            },
            "title":{
                "data_type":"String",
                "string_value":title
            },
            "content_type":{
                "data_type":"String",
                "string_value":content_type
            }
    }
    if attachments:
        m.message_attributes['attachments']={
                "data_type":"String",
                "string_value":json.dumps(attachments)
        }
    delay_seconds = max(0,min(int(send_time-time.time()),15*60))
    q.write(m,delay_seconds=delay_seconds)
    logging.info("write email %s to sqs with delay seconds: %s"%(m.message_attributes['mail_to'],delay_seconds))

def send(m):
    try:
        mail_to =  m.message_attributes.get('mail_to',{}).get('string_value')
        title = m.message_attributes.get('title',{}).get('string_value')
        content = m.get_body()
        content_type = m.message_attributes.get('content_type',{}).get('string_value','txt')
        attachments = []
        attachments_json = m.message_attributes.get('attachments',{}).get('string_value',None)
        if attachments_json:
            attachments_keys = json.loads(attachments_json)
            attachments = [MailAttachment.fromFileKeyS3(x) for x in attachments_keys]
        sendemail.send_by_mandrill(
                mail_to,
                title,
                content,
                msg_type=content_type,
                attachments=attachments)
        try:
            if attachments_json:
                attachments_keys = json.loads(attachments_json)
                for key in attachments_keys:
                    s3store.delete_file_from_s3(key)
        except:
            logging.error('delete s3 attachment error:%s'%traceback.format_exc())
        return True
    except:
        logging.error(traceback.format_exc())
    return False

def read_mail_from_sqs():
    q = get_sqs_email_queue()
    m = q.read(
            visibility_timeout=60,
            wait_time_seconds=3,
            message_attributes=[ 'send_time', 'mail_to', 'title','content_type','attachments'])
    if m:
        send_time = long(float(m.message_attributes['send_time']['string_value']))
        delay_seconds = max(0,min(int(send_time-time.time()),15*60))
        if delay_seconds<NO_DELAY_SECONDS:
            if send(m):
                logging.info("sending email to %s success and delete from sqs"%m.message_attributes['mail_to'])
                q.delete_message(m)
            else:
                q.delete_message(m)
                q.write(m,delay_seconds=60)
                logging.info("sending email to %s fail and write to sqs queue"%m.message_attributes['mail_to'])
        else:
            logging.info("rewrite email to %s to sqs with seconds:%s"%(m.message_attributes['mail_to'],delay_seconds))
            q.delete_message(m)
            q.write(m,delay_seconds=delay_seconds)
    
def write_test_msg():
    q= get_sqs_email_queue()
    m = Message()
    m.set_body('This is my first message.')
    m.message_attributes = {
        "send_time":{
            "data_type":"Number",
            "string_value":"12"
        }
    }
    q.write(m,delay_seconds=10)
    q.write(m,delay_seconds=30)
    q.write(m,delay_seconds=40)
    q.write(m,delay_seconds=50)
    q.write(m,delay_seconds=60)
    q.write(m,delay_seconds=70)

def test_get_msg():
    """
    visibility_timeout: 获取消息之后不可见时间
    wait_time_seconds: 没有消息的时候等多久返回
    """
    q = get_sqs_email_queue()
    msg = q.read(visibility_timeout=60,wait_time_seconds=3,message_attributes=['send_time'])
    if msg:
        logging.info("msg:body:%s"%msg.get_body())
        logging.info("msg_send_time:%s"%msg.message_attributes['send_time'])
        q.delete_message(msg)

def main():
    """
    一个进程不断读取sqs，然后发送邮件
    """
    while True:
        read_mail_from_sqs()

if __name__=="__main__":
    """
    已经测断开网络情况自己的重练，库写的还是比较牛逼的
    """
    main()
