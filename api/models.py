#encoding=utf8
from api import md5
from api import s3store
import pickle
import time
import json
import logging
import traceback
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class MailObject:

    def __init__(self,**kwargs):
        self.title = kwargs.get('title')
        self.mailto = kwargs.get('mailto')
        self.content = kwargs.get('content')
        self.send_time = kwargs.get('send_time')
        self.content_type = kwargs.get('content_type')
        self.attachments = kwargs.get('attachments')

    def write_to_local(self):
        """
        把邮件对象用pickle写入文件内容到本地
        """
        filename =  md5.get_md5_digest("%s,%s"%(self.mailto ,self.content))
        file_path = './mails/%s'%filename
        if not os.path.exists('./mails/'):
            os.mkdir('./mails/')
        pickle.dump(self,open(file_path,'w'))

    def __str__(self):
        return json.dumps(self.__dict__)

    @classmethod
    def read_from_file(cls,file_path):
        with open(file_path) as f:
            return pickle.load(f)

class MailAttachment:

    def __init__(self,filename,data):
        self.filename = filename
        self.data = data
        self.key = None

    def save_to_local(self):
        file_name = self.filename
        data = self.data
        #时间,文件名,以及部分内容保证不一冲突
        md5_dig = md5.get_md5_digest("%s%s%s"%(file_name,data[:1000].encode('hex'),time.time()))
        file_key =  "%s_%s"%(md5_dig,file_name)
        if not os.path.exists('./attachment/'):
            os.mkdir('./attachment/')
        f= open('./attachment/%s'%file_key,'w')
        f.write(data)
        f.close()
        self.key = file_key
        return file_key

    @classmethod
    def fromFileKeyS3(cls,file_key):
        data = s3store.get_file_from_s3(file_key)
        file_name = file_key.split("_",1)[1]
        attach = MailAttachment(file_name,data)
        attach.key = file_key
        return attach

    @classmethod
    def fromFileKeyS3(cls,file_key):
        data = s3store.get_file_from_s3(file_key)
        file_name = file_key.split("_",1)[1]
        attach = MailAttachment(file_name,data)
        attach.key = file_key
        return attach

    @classmethod
    def fromFileKey(cls,file_key):
        file_name = file_key.split("_",1)[1]
        p = './attachment/%s'%file_key
        with open(p,'r') as f:
            data = f.read()
        attach = MailAttachment(file_name,data)
        attach.key = file_key
        return attach

    @classmethod
    def delLocalFileByKey(cls,file_key):
        p = './attachment/%s'%file_key
        try:
            os.system("rm '%s'"%p)
        except:
            logging.error(traceback.format_exc())
