import hashlib
from config import SQS_KEY
from config import SQS_SECRET
from api import md5
import boto
import logging
import traceback
import time
from boto.s3.connection import S3Connection
from boto.s3.key import Key
logging.basicConfig(level=logging.INFO)

g_s3_bucket = None

def get_s3_bucket():
    global g_s3_bucket
    if g_s3_bucket:
        return g_s3_bucket
    s3_conn = S3Connection( 
                SQS_KEY,
                SQS_SECRET)
    g_s3_bucket = s3_conn.get_bucket('ariesfiles')
    return g_s3_bucket


def save_file_to_s3(file_key,data):
    bucket = get_s3_bucket()
    k = Key(bucket)
    k.key = file_key
    k.set_contents_from_string(data)
    return k.key

def get_file_from_s3(key):
    bucket = get_s3_bucket()
    k = Key(bucket)
    k.key = key
    return k.get_contents_as_string()

def delete_file_from_s3(file_key):
    bucket = get_s3_bucket()
    key_obj = bucket.get_key(file_key)
    if key_obj:
        key_obj.delete()

